# Spy Game!

This is semestral project for course **BI-IOS**.

It's an IOS app, for playing "SPY" in a company of friends using one device. 

Used technologies:
- Swift - codeBase
- SwiftUI - entire UX/UI
- MVC architecture 