//
//  CardSettingView.swift
//  spion
//
//  Created by vlad komkow on 20.01.2022.
//

import SwiftUI




struct CardSetSettingView: View {

    @Environment(\.dismiss) var dismiss
    @EnvironmentObject var cardModel: CardViewModel
    @State var isAddLocSetShown: Bool = false
    @State var newLocSetText: String = ""
    var body: some View {
        ZStack{
            VStack{
                List(){
                    ForEach(cardModel.sets, id: \.name){ m_set in
                        HStack{
                            if cardModel.sets[cardModel.activeindex].name == m_set.name{
                                Image(systemName: "checkmark")
                            }
                            else{
                                Image(systemName: "checkmark")
                                    .opacity(0)
                            }
                            LocationListView(text: m_set.name)
                            NavigationLink(destination: CardSettingView(index: cardModel.getIndex(m_set.name), newLocSet: m_set)){
                                EmptyView()
                            }
                            .isDetailLink(false)
                        
                        }
                        .listRowSeparatorTint(conf.buttonsBackgroundColor)
                        .listRowBackground(conf.backgroundColor)
                    }
                    .onDelete(perform: removeLocation)
                }
                .listStyle(.plain)
            }
            .background(conf.backgroundColor)
            newLocAlert(isShown: $isAddLocSetShown, text: $newLocSetText, title: "New Set", onDone: {
                text in
                cardModel.addSet(LocationSet(name: text, locations: []))
                
                
            })

        }
        .navigationTitle("Sets")
        .toolbar {
            ToolbarItemGroup(placement: .navigationBarTrailing){
                EditButton()
                Button(action: {
                    newLocSetText = ""
                    isAddLocSetShown = true
                    
                }){
                    Image(systemName: "plus")
                }
            }
            
            
            
        }
        
    }
    func removeLocation(at offsets: IndexSet) {
        cardModel.removeSet(atOffsets: offsets)
    }
}


struct CardSetSettingView_Previews: PreviewProvider {
    static var previews: some View {
        CardSetSettingView()
            .previewInterfaceOrientation(.portrait)
    }
}
