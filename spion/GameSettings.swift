//
//  GameSettings.swift
//  spion
//
//  Created by vlad komkow on 04.02.2022.
//

import Foundation


enum SettingsValid{
    case valid
    case error(error: String)
}

struct Settings{
    var n_players: UInt
    var n_spies: UInt
    var timer: UInt
    func isValid() -> SettingsValid{
        if n_spies >= n_players{
                return .error(error: "Add players")
        }
        return .valid
    }
}
