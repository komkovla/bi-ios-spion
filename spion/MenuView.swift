import SwiftUI


struct ContentView: View {
    @Environment(\.scenePhase) var scenePhase
    @EnvironmentObject var cardModel: CardViewModel
    @EnvironmentObject var menuModel: MenuViewModel
    @State var startActive: Bool = false
    @State var isError: Bool = false
    @State var error: String?
    let screenSize = UIScreen.main.bounds
    @State var deck: [Card] = []
    @State private var isHelpPresented = false
    var body: some View {
        NavigationView{
            
                VStack(alignment: .center){
                    firstRow()
                    Button(action: {isHelpPresented = true}){
                    fullBar(text: "How to Play")
                    }
                    .fullScreenCover(isPresented: $isHelpPresented, onDismiss: {}){
                        HelpView(isHelpPresented: $isHelpPresented)
                    }
                    MenuSegment2(tag: "Player", title: "Number of Players", conf: conf, menuModel: menuModel)
                    
                    MenuSegment2(tag: "Spy", title: "Number of Spies", conf: conf, menuModel: menuModel)
                    MenuSegment2(tag: "Timer", title: "Timer", conf: conf, menuModel: menuModel)
                    HStack{
                        //rect(opacity: 0)
                        
                        NavigationLink(destination: CardSetSettingView()){
                            fullBar(text: "Card Settings")
                            .padding(.horizontal)
                        }
                        
                        
                        //rect(opacity: 0)
                        //rect(opacity: 0)
                        //rect(opacity: 0)
                    }
                    
                    .padding(.horizontal)
                    //Start button
                    Button(action: {
                        (error ,isError) = menuModel.isValid()
                        isError.toggle()
                        if isError{
                            return
                        }
                        (error, isError) = cardModel.isValid()
                        isError.toggle()
                        if isError{
                            return
                        }
                        deck = cardModel.prepareDeck(settings: menuModel.settings)
                        startActive = true

                    }){
                        fullBar(text: "Start")
                    }
                NavigationLink(destination: gameView(model: .init(deck:  deck, timeRemaining: Int(menuModel.setting_get(tag: "Timer")))),
                               isActive: $startActive){
                        EmptyView()
                    }
                               .isDetailLink(false)
                               
        }
                .padding()
                .background(conf.backgroundColor)
                .navigationBarHidden(true)
            
        }
        .alert(error ?? "Something went wrong", isPresented: $isError){
            Button("OK",role: .cancel) {}
        }
        .onChange(of: scenePhase) { newPhase in
                        if newPhase == .background {
                            uploadSets(sets: cardModel.sets, filename: "cardSets.json")
                        }
                    }
    }
}


struct MenuSegment2: View{
    let tag: String
    let title: String
    let conf : Conf
    @ObservedObject var menuModel: MenuViewModel
    var body: some View{
        HStack{
            Button(action:{menuModel.setting_dec(tag: tag)}){
                ZStack{
                    rectBtn()
                    Image(systemName: "minus.circle")
                        .foregroundColor(conf.buttonsColor)
                        .scaleEffect(2)
                }
            }
            ZStack{
                rect3x()
                VStack{
                    Text(title)
                        .font(.headline)
                        .foregroundColor(conf.buttonsBackgroundColor)
                    Text(String(menuModel.setting_get(tag: tag)))
                        .font(.largeTitle)
                        .foregroundColor(conf.mainColor)
                }
            }
            Button(action:{menuModel.setting_inc(tag: tag)}){
                ZStack{
                    rectBtn()
                    Image(systemName: "plus.circle")
                        .foregroundColor(conf.buttonsColor)
                        .scaleEffect(2)
                }
            }
            //rect(opacity: 0)
            //rect(opacity: 0)
        }
    }
}
func fullBar(text: String) -> some View{
    ZStack{
        RoundedRectangle(cornerRadius: conf.ROUND_RADIUS)
            .fill(Conf.init().mainColor)
            .padding(.trailing, 5)
            .aspectRatio(4, contentMode: .fill)
        Text(text).font(.headline).foregroundColor(conf.buttonsColor)
    }
    .padding(2.0)
}

func firstRow() -> some View{
    HStack{
        rect()
        rectWithLetter(char: "S")
        rectWithLetter(char: "P")
        rectWithLetter(char: "Y")
        rect()
    }
}
func rectWithLetter(char: String) -> some View{
    ZStack{
        rect()
        Text(char)
            .font(.largeTitle)
            .foregroundColor(conf.backgroundColor)
    }
}
func rect3x() -> some View{
    RoundedRectangle(cornerRadius: conf.ROUND_RADIUS)
        .stroke(lineWidth: 5)
        .fill(conf.buttonsBackgroundColor)
        .padding(.trailing, 5)
        .aspectRatio(2.17, contentMode: .fill)
}
func rect3xBtn() -> some View{
    RoundedRectangle(cornerRadius: conf.ROUND_RADIUS)
        .fill(conf.mainColor)
        .padding(.trailing, 5)
        .aspectRatio(2.17, contentMode: .fill)
}
func rect(opacity: Int = 100) -> some View {
    
    RoundedRectangle(cornerRadius: conf.ROUND_RADIUS)
        .fill(conf.buttonsBackgroundColor)
        .aspectRatio(2/3, contentMode: .fit)
    //.padding(.horizontal, 2)
        .opacity(Double(opacity))
    
}
func rectBtn(opacity: Int = 100) -> some View {
    
    RoundedRectangle(cornerRadius: conf.ROUND_RADIUS)
        .fill(conf.mainColor)
        .aspectRatio(2/3, contentMode: .fit)
    //.padding(.horizontal, 2)
        .opacity(Double(opacity))
    
}




struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
