//
//  CardSettingView.swift
//  spion
//
//  Created by vlad komkow on 20.01.2022.
//

import SwiftUI


struct CardSettingView: View {
    
    let conf = Conf.init()
    @Environment(\.dismiss) var dismiss
    @EnvironmentObject var cardModel: CardViewModel
    var index: Int
    @State var isAddLocShown: Bool = false
    @State var newLocText: String = ""
    @State var isActive = false
    @State var newLocSet: LocationSet
    var body: some View {
            ZStack{
                VStack(){
                m_list(list: newLocSet.locations)
                Spacer()
            Button(action:
                        {
                            dismiss()
                            isActive = true
                        }
                ){
                    ZStack{
                        RoundedRectangle(cornerRadius: conf.ROUND_RADIUS)
                            .fill(conf.mainColor)
                            .aspectRatio(8/2, contentMode: .fit)
                        Text("Select")
                            .foregroundColor(conf.backgroundColor)
                    }
                }
                .padding()
                }
                .background(conf.backgroundColor)
            
            newLocAlert(isShown: $isAddLocShown,
                    text: $newLocText,
                    title: "New Location",
                    onDone: {
                        text in
                        newLocSet.addCard(text)
            })
            }
        
        .onDisappear(){
            cardModel.replaceSet(index: index, set: newLocSet)
            print(cardModel.sets[index].locations)
            if(isActive){
                cardModel.setActive(setName: cardModel.sets[index].name)
            }
        }
        .navigationTitle("Locations")
        .toolbar {
            ToolbarItemGroup(placement: .navigationBarTrailing){
                EditButton()
                Button(action: {
                    newLocText = ""
                    isAddLocShown = true
                }){
                    Image(systemName: "plus")
                }
            }
        }
    }
    func removeLocation(at offsets: IndexSet) {
        newLocSet.delCard(atOffsets: offsets)
    }
    
    func m_list(list : [String]) -> some View{
        List(){
            ForEach(list, id: \.self){ card in
                LocationListView(text: card)
            }
            .onDelete(perform: removeLocation)
            .listRowBackground(conf.backgroundColor)
        }
        .listStyle(.plain)
    }
}
    
    

struct CardSettingView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView{
        CardSettingView(index: 0, newLocSet: LocationSet(name: "hello", locations: ["hello", "world"]))
            .previewInterfaceOrientation(.portrait)
        }
    }
}
