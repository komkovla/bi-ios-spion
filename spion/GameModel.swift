//
//  MenuModel.swift
//  spion
//
//  Created by vlad komkow on 11.01.2022.
//
import SwiftUI
import Foundation




enum gameModes{
    case cardOpening
    case timer
}
class GameModel: ObservableObject{
    var gameMode: gameModes = .cardOpening
    @Published var timeRemaining: Int
    let arcChange: Double
    @Published var deck: [Card]
    init(deck: [Card], timeRemaining: Int){
        print("init game model")
        self.deck = deck
        self.timeRemaining = timeRemaining * 60
        self.arcChange = 360.0/Double(timeRemaining*60)
    }
    func openCard(id: String){
        for i in 0..<deck.count{
            if deck[i].id == id{
                deck[i].open()
            }
        }
    }
    func deleteCard(id: String){
        for i in 0..<deck.count{
            if deck[i].id == id{
                deck.remove(at: i)
                break
            }
        }
                if deck.count == 0{
            gameMode = .timer
        }
    }
    func anyCardOpen() -> Bool{
        for i in deck{
            if i.isOpen{
                return true
            }
        }
        return false
    }

    
    
}
