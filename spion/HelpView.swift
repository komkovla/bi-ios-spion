//
//  HelpView.swift
//  spion
//
//  Created by vlad komkow on 11.02.2022.
//

import SwiftUI

struct HelpView: View {
    @Binding var isHelpPresented: Bool
    var body: some View {
        VStack{
            ScrollView{
                VStack{
                ForEach(0..<conf.questions.count, id: \.self) { i in
                    VStack{
                    Text(conf.questions[i])
                            .font(.largeTitle)
                            .multilineTextAlignment(.center)
                            .foregroundColor(conf.mainColor)
                        Text(conf.answers[i])
                            .foregroundColor(conf.buttonsBackgroundColor)
                            .multilineTextAlignment(.center)
                    }
                    Spacer(minLength: 10)
                    
                }
                }
            }
            Spacer()
            Button(action: {isHelpPresented = false}){
                ZStack{
                    RoundedRectangle(cornerRadius: conf.ROUND_RADIUS)
                        .fill(Conf.init().mainColor)
                        .padding(.trailing, 5)
                        .aspectRatio(4, contentMode: .fit)
                    Text("Close").font(.headline).foregroundColor(conf.backgroundColor)
                }
            }
        }
        .padding()
        .background(conf.backgroundColor)
    }
}

struct HelpView_Previews: PreviewProvider {
    static var previews: some View {
        HelpView(isHelpPresented: .constant(true))
    }
}
