//
//  Settings.swift
//  spion
//
//  Created by vlad komkow on 27.01.2022.
//

import Foundation
import SwiftUI
struct Conf{
    let mainColor: Color = Color(red: 217.0/256.0, green: 4.0/256.0, blue: 41.0/256.0)
    let buttonsColor: Color = Color(red: 43.0/256.0, green: 45.0/256.0, blue: 66.0/256.0)
    let ROUND_RADIUS: CGFloat = 10
    let backgroundColor = Color(red: 43.0/256.0, green: 45.0/256.0, blue: 66.0/256.0)
    let buttonsBackgroundColor = Color(red: 160.0/256.0, green: 3.0/256.0, blue: 29.0/256.0)
    //160, 3, 29
    let questions = ["Where to start?",
                     "What next?",
                     "Can another player see my card?",
                     "How to play?",
                     "What to do after you have guess the Spy?",
                     "I am Spy and i know the location!",
                     "Why there is a timer?"]
    let answers = ["Choose settings: a word set, time, number of Players and Spies.",
                   "There are two types of roles: locals and spies. Pass the phone around so everyone can get their role.",
                   "No, they can't. When you tap on the card, it flips over. On the other side there is information about whether you are a spy, and if you are not, there is a location. After you tap once again the card will disappear and you will see the cover of the next card. Pass the phone to the next player so they can flip the card over and get their role.",
                   "After everyone has got their role, start Playing. Ask each other questions Connected with the location from the Locals' cards. For example: «what is the Color of this place?», «how often do you Go there?», «is this a nice place to visit?» The locals should expose the spy - the Person who doesn't understand what Location the others are talking about. The spy should act as if they knew the Location and at the same time they Should try to guess it.",
                   "Say: «I know who the spy is.» then on the count of three all the players should point at the person who is the spy, in their view. If all the players have chosen the same person, this person has to reveal their role. If they are a local, the spy wins. If they are the spy, the locals win. If the players have pointed at different people, the game continues.",
                   "Just say: «I know the location» and then name it. If you are right - you win. If you are wrong - the locals win.",
                   "If the locals find out who the spy is before the time is up, they win. If the spy manages to stay unexposed for the whole time - they win."
    ]

}
let conf: Conf = .init()
