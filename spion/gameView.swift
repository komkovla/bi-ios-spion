//
//  gameView.swift
//  spion
//
//  Created by vlad komkow on 20.01.2022.
//

import SwiftUI

func calcArcChange(){
    
}


struct gameView: View {
    @Environment(\.dismiss) var dismiss
    @ObservedObject var model: GameModel
    @State var arcAngle: Double = 0.01
    var body: some View {
        
        switch model.gameMode{
        case .cardOpening:
            VStack{
            ScrollView{
                    HStack{
                        LazyVGrid(columns:[GridItem(.adaptive(minimum: 100))]){
                            ForEach(model.deck, id: \.id){ card in
                            CardView(card: card)
                                .aspectRatio(2/3, contentMode: .fit)
                                .onTapGesture {
                                    if card.isOpen{
                                        withAnimation{
                                            model.deleteCard(id: card.id)
                                        }
                                    }
                                    else{
                                        if !model.anyCardOpen(){
                                            withAnimation(.easeInOut){
                                            model.openCard(id: card.id)
                                            }
                                            
                                        }
                                    }
                                }
                                
                        }
                        }
                    }
                    
                    }
                Text("Tap to open card\nTap again to discard")
                    .multilineTextAlignment(.center)
                    .foregroundColor(conf.mainColor)
                    .clipShape(Capsule())
                    
            }
            
            .padding()
            .background(conf.backgroundColor)
            
        case .timer:
            let timeRemaining = model.timeRemaining
            let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
            ZStack{
                Pie(subStruct: arcAngle)
                    //.opacity(0.3)
                    .padding()
                    .foregroundColor(conf.mainColor)
                    .animation(.linear(duration: 1.0), value: arcAngle)
                Circle()
                    .scaleEffect(0.5)
                    .foregroundColor(conf.backgroundColor)
                Text(String(format: "%d:%d", arguments: [timeRemaining/60, timeRemaining%60]))
                    .font(.largeTitle)
                    .foregroundColor(conf.mainColor)
                                .padding(.horizontal, 5)
                                .padding(.vertical, 5)
                        
                .onReceive(timer) { time in
                    if timeRemaining > 0 {
                        model.timeRemaining -= 1
                        if arcAngle < 360{
                            //withAnimation(.linear(duration: 1)){
                                arcAngle += model.arcChange
                            //}
                        }
                    }
                    else {
                        dismiss()
                    }
                }
                    
            }
            .background(conf.backgroundColor)
        }
        
    }
}

struct CardView: View{
    let card: Card
    
    var body: some View{
        ZStack {
            RoundedRectangle(cornerRadius: 15)
                            .fill(conf.backgroundColor)
                            
            let rect = RoundedRectangle(cornerRadius: 15)
                rect
                .stroke(lineWidth: 5)
                .foregroundColor(conf.mainColor)
                //rect.scaleEffect(3, anchor: .leading)
                if card.isSpy{
                    Text("Spy")
                        .font(.largeTitle)
                        .foregroundColor(conf.mainColor)
                        .zIndex(1)
                }
                else{
                Text(card.location)
                        
                        .font(.body)
                        .bold()
                    .foregroundColor(conf.mainColor)
                    .zIndex(1)
                }
            if(!card.isOpen){
                RoundedRectangle(cornerRadius: 15)
                                .fill(conf.mainColor)
                                .zIndex(1)
            }
            
        }
        .padding(.all, 3)
        .foregroundColor(.red)
        
    }
}





struct gameView_Previews: PreviewProvider {
    static var previews: some View {
        gameView(model: .init(deck: [Card(location: "hello"), Card(location: "world")], timeRemaining: 1))
    }
}
