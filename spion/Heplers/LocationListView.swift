//
//  LocationListView.swift
//  spion
//
//  Created by vlad komkow on 11.02.2022.
//

import SwiftUI

struct LocationListView: View{
    let text: String
    var body: some View{
        ZStack{
            RoundedRectangle(cornerRadius: 15)
                .fill(conf.mainColor)
            Text(text)
                .foregroundColor(conf.buttonsColor)
        }
    }
}

struct LocationListView_Previews: PreviewProvider {
    static var previews: some View {
        LocationListView(text: "set1")
    }
}
