//
//  newLocAlert.swift
//  spion
//
//  Created by vlad komkow on 27.01.2022.
//

import SwiftUI

extension UIApplication {
    func endEditing() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}

struct newLocAlert: View {
    let screenSize = UIScreen.main.bounds
    @Binding var isShown: Bool
    @Binding var text: String
    var title: String
    var onCancel: () -> Void = { }
    var onDone: (String) -> Void = { _ in }
    var body: some View {
        VStack(alignment: .center, spacing: 20){
            Text(title)
        TextField("New location", text: $text)
                .textFieldStyle(RoundedBorderTextFieldStyle())
            HStack(){
                
                Button(action: {
                    endEditing()
                    onCancel()
                    isShown = false
                }){
                    Text("Zpět")
                    
                }
                Spacer()
                Button(action: {
                    isShown = false
                    endEditing()
                    guard text.isEmpty else {
                        onDone(text)
                        return
                    }
                }){
                    Text("Přidat")
                }
            }
            .padding(.horizontal, 40)
        }
        
            .padding()
                    .frame(width: screenSize.width * 0.7, height: screenSize.height * 0.2)
                    .background(Color(red: 141.0/256, green: 153.0/256, blue: 174/256))
                    .clipShape(RoundedRectangle(cornerRadius: 20.0, style: .continuous))
                    //.shadow(color: .gray, radius: 10)
                    .offset(y: isShown ? 0 : screenSize.height)
                    .animation(.spring(), value: isShown)
        
    }
    private func endEditing() {
        UIApplication.shared.endEditing()
    }
}

struct newLocAlert_Previews: PreviewProvider {
    static var previews: some View {
        newLocAlert(isShown: .constant(true), text: .constant(""), title: "Hello")
    }
}
