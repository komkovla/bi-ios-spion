//
//  pie.swift
//  spion
//
//  Created by vlad komkow on 10.02.2022.
//

import SwiftUI


struct Pie: Shape{
    var subStruct: Double
    
    var animatableData: Double{
        get { subStruct }
        set { subStruct = newValue}
    }
    
    
    func path(in rect: CGRect) -> Path {
        let center: CGPoint = CGPoint(x: rect.midX, y: rect.midY)
        let radius = min(rect.width, rect.height) / 2
        var p = Path()
        p.move(to: center)
        p.addLine(to: CGPoint(x: center.x, y: center.y + radius))
        p.addArc(center: center, radius: radius, startAngle: Angle(degrees: 0-90), endAngle: Angle(degrees: subStruct-90), clockwise: true)
        p.addLine(to: center)
        return p
    }
}


struct Pie_Previews: PreviewProvider {
    static var previews: some View {
        Pie(subStruct: 90)
    }
}
