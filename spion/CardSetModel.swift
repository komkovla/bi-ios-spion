//
//  CardSetModel.swift
//  spion
//
//  Created by vlad komkow on 08.02.2022.
//

import Foundation
import SwiftUI

func loadSets(_ filename: String) -> [LocationSet]{
    print("init sets")
    var data: Data
    
    guard var file = Bundle.main.url(forResource: filename, withExtension: nil)
    else {
        fatalError("Couldn't find \(filename) in main bundle.")
    }
    do{
        file = getDocumentDirectory(filename)
        print("sets found : \(file)")
        data = try Data(contentsOf: file)
    }
    catch{
        file = Bundle.main.url(forResource: filename, withExtension: nil)!
        print("no prev found")
    }
    do{
        data = try Data(contentsOf: file)
    }
    catch{
        fatalError("Couldn't read \(filename) in main bundle.")
    }
        
    do {
        let decoder = JSONDecoder()
        print("read from \(file)")
        return try decoder.decode([LocationSet].self, from: data)
    } catch {
        fatalError("Couldn't parse \(filename) as \([LocationSet].self):\n\(error)")
    }
}

func getDocumentDirectory(_ filename: String) -> URL {
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    var documentsDirectory = paths[0]
    documentsDirectory = documentsDirectory.appendingPathComponent("\(filename)")
    return documentsDirectory
}

func uploadSets(sets:[LocationSet], filename: String){
    let data : Data
    //let pathWithFileName = documentDirectory.appendingPathComponent("/Resoueses/cardSets")
    let encoder = JSONEncoder()
    do{
        try data = encoder.encode(sets)
    }
    catch{
        fatalError("Couldn't encode")
    }

    do{
        //jsonToString(json: data)
        let file = getDocumentDirectory(filename)
        try data.write(to: file)
        print("write to \(getDocumentDirectory(filename))")
    }
    catch{
        fatalError("Couldn't write\n\(error)")
    }
}



struct Card:Identifiable, Hashable{
    var id = UUID().uuidString
    var isOpen: Bool = false
    var isSpy: Bool = false
    let location: String
    mutating func open(){
        isOpen = true
    }
}


struct LocationSet{
    private(set) var isActive = false
    private(set) var name: String
    private(set) var locations: [String]
    func isEmpty() -> Bool{
        return locations.isEmpty
    }
    func getRandomLoc() -> String{
        return locations.randomElement()!
    }
    
    
    mutating func addCard(_ location: String) {
        locations.append(location)
    }
    mutating func delCard(_ location: String) {}
    mutating func delCard(atOffsets: IndexSet){
        locations.remove(atOffsets: atOffsets)
    }
    mutating func setActive(){
        isActive = true
    }
    mutating func clearActive(){
        isActive = false
    }
}

extension LocationSet: Codable {
    enum CodingKeys: String, CodingKey {
        case name
        case locations
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decode(String.self, forKey: .name)
        locations = try container.decode([String].self, forKey: .locations)
    }
}




class CardViewModel: ObservableObject{
    @Published var sets: [LocationSet]
    @Published var activeindex: Int = 0
    @Published var m_init = 0
    
    init(){
        sets = [LocationSet(name: "set1", locations: ["card1", "card2"]), LocationSet(name: "set2", locations: ["card3", "card4"])]
        //sets = loadSets("cardSets.json")
        if self.sets.first != nil{
            self.sets[0].setActive()
        }
    }
    func removeSet(atOffsets: IndexSet){
        //TODO: check if active
        sets.remove(atOffsets: atOffsets)

    }
    func replaceSet(index: Int, set: LocationSet){
        sets[index] = set
    }
    
    
    func setActive(setName: String){
        sets[activeindex].clearActive()
        for i in (0..<sets.count){
            if sets[i].name == setName{
                sets[i].setActive()
                activeindex = i
                print("active: \(activeindex)")
            }
        }
    }
    
    func addSet(_ set: LocationSet){
        self.sets.append(set)
        
    }
    
    func getIndex(_ name : String) -> Int{
        for i in (0..<sets.count){
            if sets[i].name == name{
                return i
            }
        }
        return 0
    }
    
    func isValid() -> (String, Bool){
        for i in sets{
            if i.isActive{
                if i.isEmpty(){
                    return ("Add location", false)
                }
                return ("", true)
            }
        }
        return ("No active deck", false)
    }
    func prepareDeck(settings: Settings) -> [Card]{
        let location = sets[activeindex].getRandomLoc()
        var deck: [Card] = []
        for _ in 0..<settings.n_players{
            deck.append(Card(location: location))
        }
        var spies_placed = 0
        while spies_placed < settings.n_spies{
            let random = Int.random(in: 0..<deck.count)
            if deck[random].isSpy{
                continue
            }
            else{
                print("Game init")
                spies_placed+=1
                deck[random].isSpy = true
            }
        }
        return deck
    }
    
    func getCards() -> LocationSet {
        return sets[activeindex]
    }
    
}



