import Foundation
import SwiftUI




class GameViewModel{
    private var deck: [Card]
    init(deck: [Card]){
        self.deck = deck
    }
    func getDeck() -> [Card]{
        return deck
    }
}





class MenuViewModel: ObservableObject{
    //@Published var cardModel : CardViewModel
    @Published var settings: Settings
    init(players: UInt, spies: UInt, timer: UInt){
        self.settings = Settings(n_players: players, n_spies: spies, timer: timer)
        print("init menu")
    }
    func setting_inc(tag: String){
        switch tag{
        case "Player":
            settings.n_players+=1
        case "Spy":
            settings.n_spies+=1
        case "Timer":
            settings.timer+=1
        default:
            break
        }
    }
    func setting_get(tag: String)-> UInt{
        switch tag{
        case "Player":
            return settings.n_players
        case "Spy":
            return settings.n_spies
        case "Timer":
            return settings.timer
        default:
            return 0
        }
    }
    func setting_dec(tag: String){
        switch tag{
        case "Player":
            if settings.n_players > 1{
            settings.n_players-=1
            }
        case "Spy":
            if settings.n_spies > 1{
            settings.n_spies-=1
            }
        case "Timer":
                if settings.timer > 1{
            settings.timer-=1
                }
        default:
            break
        }
    }
    func isValid() -> (String, Bool){
        var m_error : SettingsValid
        m_error = settings.isValid()
        switch m_error {
        case .valid:
            break
        case .error(let error):
            return (error, false)
        }
        return ("", true)
    }
}
