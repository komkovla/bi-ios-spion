//
//  spionApp.swift
//  spion
//
//  Created by vlad komkow on 11.01.2022.
//

import SwiftUI

@main
struct spionApp: App {
    @StateObject var cardModel: CardViewModel = .init()
    @StateObject var menuModel: MenuViewModel = MenuViewModel(players: 5, spies: 1, timer: 2)
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(cardModel)
                .environmentObject(menuModel)
        }
    }
}
